
# RTF API Named - checkinapiintegration

Migrated from SVN - https://svm.virginblue.com.au/itdev/CheckinApiIntegeration
 
## Intro

Existing project named "CheckinApiIntegeration" an API Layer of Ready To Fly is migrated from SVN to Gitlab SAAS.
Migration includes :
  * Gitlab Project creation
  * Migration of OLD Commits history of SVN.
  * Migration of Jenkins CI/CD pipeline to Gitlab CI.
  * Automated Release and Publishing of artifact to Nexus.

## Contents

*   [How to migrate from SVN ?](#how-tomigrate-from-svn)
*   [How to push to gitlab ?](#how-to-push-to-gitlab)
*   [How to add CI/CD pipeline ?](#how-to-add-ci-pipeline)
*   [How to build and deploy ?](#how-to-build-and-deploy)
*   [How to automat release process ?](#how-to-automate-release-process)
*   [How to deploy application ?](#how-to-deploy)
*   [Conclusion](#conclusion)

## How to migrate from SVN ?

Follow given below instruction to migrate a branch from SVN to gitlab feature branch :
  * Use 'git-svn' tool to convert repos from SVN to GIT.
    ```
    export HTTPS_PROXY=https://proxy.virginblue.internal:8080
    export HTTP_PROXY=http://proxy.virginblue.internal:8080
    git config --global init.defaultBranch <gitlab target branch name>
    #i.e git config --global init.defaultBranch features/webcheck-overview
    git svn clone  https://svn.virginblue.com.au/ResponsiveIBE/webcheck/branches/<branch-name>/
    ```
  * Add following CI/CD configuration files as apart of source code:

    ```
    1)  .gitlab-ci.yml         - This is the yaml based CI/CD jobs/stages definition.
    2)   Makefile.             - Defination of CI/CD jobs.
    3)  .mvn/maven.config      - This file for maintaining POM file versioning.
    4)  .bumpversion.cfg       - This file to maintain auto increment release version of pom file.
    5)  .gitignore             - This file used to ignore local file changes i.e .project , .settings etc.
    6)   Pom.xml                - Update existing pom with adding ${release} var in <version>.
    ```

## How to push to gitlab ?

   Push the checkout branch from SVN to Gitlab repo as target branch :

    unset HTTPS_PROXY
    unset HTTP_PROXY
    git remote add origin https://gitlab.com/virginaustralia/cc/check-in/webcheck.git 
    git branch -M features/<branch name>

    
## How to add CI/CD pipeline ?

  To add pipeline-as-a-code in this project , needs to add & define .gitlab-ci.yml & Makefile.
  Using these stages in the pipeline : Build -> Test -> Release -> Deploy.See files definition in the project:

  ![Diagram](doc/gitlab-ci.jpg)

## How to build and deploy ?
 
  Using Gitlab Flow for creation of branches and their respective pipelines.Using Gitlab runner as 'Docker Executor" to use base builder docker image (JDK 8 & MVN 3 ).
  Based on VA recommended branchinging strategy , "Features/<branch_name>" -> "Develop" -> "Master" -> "TAGs". See below branch based pipelines screenshot:

  ![Diagram](doc/release_pipeline.jpg)
  

## How to automat release process and publish artefact?

  To auto increment generated artifact release version , using this file “.bumpversion.cfg” and defined “patch” stage in makefile to update the version and auto increment by 1.
  After creating TAG, artifact published to VA Nexus during “release:perform” stage in the pipeline :
   ```
     <distributionManagement>
        <repository>
          <id>va-releases</id>
          <name>VA Release Nexus Repository</name>
          <url>https://nexus.virginaustralia.com/repository/va-releases/</url>
        </repository>
          <snapshotRepository>
          <id>va-snapshots</id>
          <name>VA Snapshots Nexus Repository</name>
          <url>https://nexus.virginaustralia.com/repository/va-snapshots/</url>
          <uniqueVersion>true</uniqueVersion>
        </snapshotRepository>
     </distributionManagement>

   ```

## How to deploy application ?

In order to deploy WARs on target server named "wwwmgmt" server , setup SSH based connection between Gitlab Runners & MGMT server using OpenSSH connection : 

 ```
  before_script:
    # Setup SSH deploy keys
     - 'command -v ssh-agent >/dev/null || ( apk add --update openssh )' 
     - eval $(ssh-agent -s) 
     - echo "$DEPLOY_SERVER_PRIVATE_KEY" | tr -d '\r' | ssh-add - 
     - mkdir -p ~/.ssh - chmod 700 ~/.ssh 
     - ssh-keyscan $MGMT_SERVER >> ~/.ssh/known_hosts 
     - chmod 644 ~/.ssh/known_hosts  
 ```

## Conclusion


